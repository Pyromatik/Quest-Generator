import random
import tkinter as tk

# arrays
presets = ["gather","slay","find"]
gathering= ["Retrieve the ancient artifact from the ruins of the lost city.",
                "Gather berries from the nearby Forrest that is infestet with monsters",
                "grab a specific item from the market."]
slaying= ["Slay a greater Monster and claim its treasure.",
                "Kill a hord of monsters nearby the town or city",
                "assasinate a person of authority."]
finding= ["find an artifact in old ruins.",
                "find a relative of the quest giver",
                "find your mom."]
difficulties = [1,2,3,4,5,6,7,8,9]

def generate_quest():
    # Select a random quest
    preset = random.choice(presets)
    if preset == "gather":
        description = random.choice(gathering)
    if preset == "slay":
        description = random.choice(slaying)
    if preset == "find":
        description = random.choice(finding)
    difficulty = random.choice(difficulties)

    reward = 0

    # add difficulty to reward
    if difficulty <= 3:
        reward += 50
    elif difficulty <= 6:
        reward += 100
    elif difficulty > 6:
        reward += 200

    # add modifiers to reard
    if "monsters" in description:
        reward *= 1.5
    if "assasinate" in description:
        reward *= 2

    # Update the output fields
    preset_field.config(text=preset)
    description_field.config(text=description)
    difficulty_field.config(text=difficulty)
    reward_field.config(text=reward)

# Create the main window
root = tk.Tk()
root.title("Quest Generator")

# Create the output fields
tk.Label(root, text="Preset:").grid(row=0, column=0)
preset_field = tk.Label(root, text="")
preset_field.grid(row=0, column=1)

tk.Label(root, text="Description:").grid(row=1, column=0)
description_field = tk.Label(root, text="")
description_field.grid(row=1, column=1)

tk.Label(root, text="Difficulty:").grid(row=2, column=0)
difficulty_field = tk.Label(root, text="")
difficulty_field.grid(row=2, column=1)

tk.Label(root, text="Reward:").grid(row=3, column=0)
reward_field = tk.Label(root, text="")
reward_field.grid(row=3, column=1)

# Create the generate button
generate_button = tk.Button(root, text="Generate", command=generate_quest)
generate_button.grid(row=4, column=0, columnspan=2)

# Start the main loop
root.mainloop()
